(ns stunners-app.validation
  (:require [clojure.string :as s]))

(defmulti error-message
  "Returns an error message given a spec problem. Returns ::unknown-error when there is no matching multimethod fn. Dispatches on the first element of :path."
  (fn [{[fpath] :path}] fpath))

(defmethod error-message :location/lat [problem]
  "Location was not provided")
(defmethod error-message :location/lng [problem]
  "Location was not provided")

(defmethod error-message :appointment/time [{:keys [val]}]
  (cond
    (nil? val) "Appointment time was not provided"
    :else "Invalid appointment time"))

(defmethod error-message :appointment/product-types [_]
  "You must specify at least 1 product")

(defmethod error-message :user/name [{:keys [val]}]
  (cond
    (s/blank? val) "User name was not provided"
    :else (str val " is not a valid user name")))

(defmethod error-message :user/email [{:keys [val]}]
  (cond
    (s/blank? val) "Email address was not provided"
    :else (str val " is not a valid email address")))

(defmethod error-message :user/phone-number [{:keys [val]}]
  (cond
    (s/blank? val) "Phone number was not provided"
    :else (str val " is not a valid phone number")))

(defmethod error-message :default [_]
  ::unknown-error)

(defn spec-error->validation-messages [spec-error]
  (println spec-error)
  (->> (:clojure.spec.alpha/problems spec-error)
       (map error-message)
       distinct))
