(ns stunners-app.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [cljs-exponent.reagent :refer [app-registry text]]
            [stunners-app.handlers]
            [stunners-app.subs]
            [stunners-app.pages.home-page :as home]
            [stunners-app.styles :as styles]
            [cljs-react-navigation.re-frame :as nav]
            [stunners-app.native-base :as nb]
            [stunners-app.pages.booking :as booking]
            [stunners-app.react-native :refer [add-back-handler]]
            [re-frame.loggers :as rf.log]))

(enable-console-print!)

(js/require "react-navigation")
(js/require "@expo/vector-icons")
(def expo (js/require "expo"))

(def navigation-stack
  (nav/stack-navigator {:home {:screen (nav/stack-screen home/page {:header nil})}
                        :book-appointment {:screen (nav/stack-screen booking/page {:header nil})}}
                       {:headerMode "none"}))

(defn app-root []
  (fn []
    [nb/root
     [nav/router {:root-router navigation-stack}]]))


(defn init []
  (add-back-handler
   (fn []
     (let [routes (-> (rf/subscribe [:cljs-react-navigation.re-frame/routing-state])
                      deref
                      (js->clj :keywordize-keys true)
                      :routes)]
       (if (< 1 (count routes))
         (do (rf/dispatch [:cljs-react-navigation.re-frame/goBack])
             true)
         false))))
  (rf/dispatch-sync [:initialize])
  (.registerRootComponent expo (r/reactify-component app-root)))


(defn reload []
  (def warn (js/console.warn.bind js/console))
  (rf.log/set-loggers!
    {:warn (fn [& args]
             (cond
               (= "re-frame: overwriting " (first args)) nil
               :else (apply warn args)))}))
