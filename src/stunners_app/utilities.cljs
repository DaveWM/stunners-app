(ns stunners-app.utilities
  (:require [stunners-app.react-native :as rn]
            [clojure.string :as str]))

(defn to-js [m]
  "Converts a map with keyword keys into a js object. Keeps the namespace of the keys"
  (->> m
       (map #(->> %
                  (map (fn [[k v]]
                         [(subs (str k) 1) v]))
                  (into {})))
       clj->js))

(defn to-data-source [data]
  "converts a vector/list into a data source"
  (-> (rn/data-source. (clj->js {:rowHasChanged not=}))
      (.cloneWithRows (to-js data))))

(defn split-comma [s]
  (str/split s #","))

(defn to-query-string [params]
  (str "?" (->> params
                (map (fn [[k v]]
                       (str (name k) "=" (js/encodeURIComponent v))))
                (str/join "&"))))
