(ns stunners-app.handlers
  (:require
    [re-frame.core :refer [reg-event-db reg-event-fx reg-fx reg-cofx ->interceptor dispatch]]
    [stunners-app.db :as db :refer [app-db app-datom-id]]
    [datascript.core :as d]
    [cljs-exponent.font :as exp-font]
    [re-play.core :as rp]
    [clojure.set :refer [union difference intersection]]
    [stunners-app.native-base :as nb]
    [cljs-exponent.reagent :as expo]
    [stunners-app.expo-helpers :refer [start-auth-session get-redirect-url]]
    [stunners-app.utilities :as utils]
    [stunners-app.config :refer [api-url google-maps-key]]
    [cljs.reader]
    [stunners-app.jwt :as jwt]
    [cemerick.url :refer [url]]
    [stunners-app.validation :as validation]
    [stunners-app.react-native :as rn]))

(def auth-token-key "auth-token")

;; -- Handlers --------------------------------------------------------------

(reg-fx
  :load-fonts
  (fn [{:keys [fonts done]}]
    (-> fonts
        exp-font/load-async
        (.then #(when done (dispatch done)))
        (.catch #(println %)))))

(reg-fx
  :toast
  (fn [{:keys [text]}]
    (.show nb/Toast (clj->js {:text text
                              :position "bottom"
                              :duration 5000
                              :buttonText "dismiss"}))))

(reg-fx
  :get-token
  (fn [{:keys [done]}]
    (-> (.getItem expo/async-storage auth-token-key)
        (.then (fn [token]
                 (dispatch [done token]))))))

(reg-fx
  :authenticate
  (fn [_]
    (let [base-url     "https://dwmartin41.eu.auth0.com/authorize"
          query-string (utils/to-query-string {:client_id "Uc6xSnmrrj9L155vsIrpAhXrHnUMGX6w"
                                               :response_type "token id_token"
                                               :scope "openid profile name picture email phone_number"
                                               :audience "https://stunners-backend.herokuapp.com"
                                               :nonce "fuck off"
                                               :redirect_uri (get-redirect-url)})]
      (-> (start-auth-session {:authUrl (str base-url query-string)})
          (.then (fn [result]
                   (let [params (aget result "params")
                         [access-token id-token] (->> ["access_token" "id_token"]
                                                      (map #(aget params %)))]
                     (.setItem expo/async-storage auth-token-key access-token)
                     (dispatch [:token-retrieved access-token id-token]))))
          (.catch #(dispatch [:token-retrieved nil]))))))

(defn get-auth-token [db]
  (d/q '[:find ?token .
         :where [0 :auth/token ?token]]
       db))

(defn add-auth-token [request db]
  (if-let [token (get-auth-token db)]
    (assoc-in request [:headers "Authorization"] (str "Bearer " token))
    request))

(defn edn-request [request]
  (-> request
      (assoc-in [:headers "Content-Type"] "application/edn")
      (assoc-in [:headers "Accept"] "application/edn")
      (assoc :format pr-str)
      (assoc :response-format cljs.reader/read-string)))

(defn handle-not-authorised [request]
  (assoc-in request [:on-response-code 401] [:token-not-valid]))

(defn json-request [request]
  (letfn [(parse-json [json-string]
            (-> (.parse js/JSON json-string)
                (js->clj :keywordize-keys true)))]
    (assoc request :response-format parse-json)))

(defn update-when [m k f]
  (if (get m k)
    (update m k f)
    m))

(reg-fx
  :fetch
  (fn [{:keys [format response-format on-success on-failure uri on-response-code] :as request}]
    (-> (js/fetch uri (-> request
                          (update-when :body format)
                          clj->js))
        (.then (fn [response]
                 (-> (.text response)
                     (.then response-format)
                     (.then (fn [response-data]
                              (let [status           (.-status response)
                                    handler-for-code (get on-response-code status)
                                    handler          (cond
                                                       handler-for-code handler-for-code
                                                       (.-ok response) on-success
                                                       on-failure on-failure)]
                                (when handler
                                  (dispatch (conj handler response-data status)))))))))
        (.catch #(println %)))))

(reg-fx
  :open-url
  (fn [url]
    (rn/open-url url)))

(reg-event-fx
  :initialize
  [rp/recordable]
  (fn [_ _]
    {:db (assoc app-db :routing (clj->js {:index 0
                                          :routes [{:key "home" :routeName :home}]}))
     :load-fonts {:fonts (merge {"elsie-regular" (js/require "./assets/fonts/Elsie-Regular.ttf")
                                 "elsie-black" (js/require "./assets/fonts/Elsie-Black.ttf")
                                 "fs-lola" (js/require "./assets/fonts/fs_lola.ttf")
                                 "Roboto" (js/require "native-base/Fonts/Roboto.ttf")
                                 "Roboto_medium" (js/require "native-base/Fonts/Roboto_medium.ttf")})
                  :done [:fonts-loaded]}
     :get-token {:done :token-retrieved}}))

(reg-event-db
  :fonts-loaded
  [rp/recordable]
  (fn [db]
    (d/db-with db [[:db/add app-datom-id :app/fonts-loaded true]])))

(reg-event-fx
  :token-retrieved
  [rp/recordable]
  (fn [{:keys [db]} [_ access-token id-token]]
    (let [updated-db (if access-token
                       (d/db-with db [[:db/add app-datom-id :auth/token access-token]
                                      [:db/add app-datom-id :app/loading true]])
                       db)]                                 ; maybe retract current token fact?
      (merge {:db updated-db}
             (when access-token
               {:fetch (-> {:method :get
                            :uri (str api-url "/user")
                            :on-success [:user-received]
                            :on-failure [:token-not-valid]
                            :on-response-code {404 [:user-not-found id-token]}}
                           (add-auth-token updated-db)
                           (edn-request)
                           (handle-not-authorised))})))))

(reg-event-db
  :token-not-valid
  [rp/recordable]
  (fn [db]
    (when-let [token (get-auth-token db)]
      (-> db
          (d/db-with [[:db/retract app-datom-id :auth/token token]
                      [:db/add app-datom-id :app/loading false]])))))

(reg-event-fx
  :user-received
  [rp/recordable]
  (fn [{:keys [db]} [_ {:keys [db/id] :as response}]]
    {:db (d/db-with db (cons response
                             [[:db/add app-datom-id :app/current-user id]]))
     :fetch (-> {:method :get
                 :uri (str api-url "/stylists")
                 :on-success [:stylists-received]}
                (add-auth-token db)
                (edn-request)
                (handle-not-authorised))}))

(reg-event-fx
  :stylists-received
  [rp/recordable]
  (fn [{:keys [db]} [_ {:keys [main related]}]]
    {:db (d/db-with db (concat main related))
     :fetch (-> {:method :get
                 :uri (str api-url "/appointments")
                 :on-success [:appointments-received]}
                (add-auth-token db)
                (edn-request)
                (handle-not-authorised))}))

(reg-event-db
  :appointments-received
  [rp/recordable]
  (fn [db [_ {:keys [main related]}]]
    (d/db-with db (concat main related [[:db/add app-datom-id :app/loading false]
                                        [:db/add app-datom-id :existing-appointments/refreshing false]]))))

(reg-event-fx
  :user-not-found
  [rp/recordable]
  (fn [{:keys [db]} [_ id-token response]]
    (if id-token
      (let [{:keys [email] name :nickname phone-number :phone_number avatar :picture :as decoded-token} (jwt/decode id-token)
            transactions (->> {:name name
                               :phone-number phone-number
                               :avatar avatar
                               :email email}
                              (remove (fn [[k v]]
                                        (nil? v)))
                              (map (fn [[k v]]
                                     [:db/add app-datom-id (keyword :registration k) v]))
                              vec)]
        {:db (d/db-with db (conj transactions [:db/add app-datom-id :app/loading false]))})
      {:db (d/db-with db [[:db/add app-datom-id :app/loading false]])})))


(reg-event-db
  :registration/email-changed
  [rp/recordable]
  (fn [db [_ email]]
    (d/db-with db [[:db/add app-datom-id :registration/email email]])))

(reg-event-db
  :registration/name-changed
  [rp/recordable]
  (fn [db [_ name]]
    (d/db-with db [[:db/add app-datom-id :registration/name name]])))

(reg-event-db
  :registration/phone-number-changed
  (fn [db [_ phone-number]]
    (d/db-with db [[:db/add app-datom-id :registration/phone-number phone-number]])))

(reg-event-db
  :registration/terms-accepted-changed
  (fn [db _]
    (let [current-state (d/q '[:find ?accepted .
                               :where [0 :registration/terms-accepted ?accepted]]
                             db)]
      (d/db-with db [[:db/add app-datom-id :registration/terms-accepted (not current-state)]]))))

(reg-event-fx
  :registration/register
  [rp/recordable]
  (fn [{:keys [db]}]
    (let [{:keys [registration/name registration/email registration/avatar registration/phone-number]} (d/entity db app-datom-id)]
      {:fetch (-> {:method :post
                   :uri (str api-url "/user")
                   :body {:user/name name
                          :user/email email
                          :user/avatar avatar
                          :user/phone-number phone-number}
                   :on-success [:user-received]
                   :on-failure [:registration/failed]}
                  (add-auth-token db)
                  edn-request
                  handle-not-authorised)
       :db (d/db-with db [[:db/add app-datom-id :app/loading true]])})))

(reg-event-fx
  :registration/failed
  [rp/recordable]
  (fn [{:keys [db]} [_ {:keys [explanation]}]]
    (prn explanation)
    (let [validation-messages (->> explanation
                                   validation/spec-error->validation-messages
                                   (map (fn [e]
                                          (if (= e ::validation/unknown-error)
                                            "Unknown error - please contact support"
                                            e))))]
      {:db (d/db-with db [[:db/add app-datom-id :registration/errors validation-messages]
                          [:db/add app-datom-id :app/loading false]])})))

(reg-event-fx
  :log-in
  [rp/recordable]
  (fn [{:keys [db]} _]
    {:db db
     :authenticate {}}))

(reg-event-db
  :loaded-fonts
  [rp/recordable]
  (fn [db _]
    (println "loaded")
    (d/db-with db [[:db/add app-datom-id :app/loading false]])))

(reg-event-db
  :select-filter-group
  [rp/recordable]
  (fn [db [_ id]]
    (d/db-with db [[:db/add app-datom-id :new-appointment/selected-filter-group id]])))

(reg-event-fx
  :start-booking
  [rp/recordable]
  (fn [{:keys [db]} [_ stylist-id selected-products]]
    {:db (-> db
             (d/db-with (concat [[:db/add app-datom-id :booking/stylist-id stylist-id]]
                                (map #(vec [:db/add app-datom-id :booking/selected-products %]) selected-products)))
             (update :routing #(-> %
                                   js->clj
                                   (update "routes" conj {:key :book-appointment :routeName :book-appointment})
                                   (update "index" inc)
                                   clj->js)))}))

(reg-event-db
  :booking/product-toggled
  [rp/recordable]
  (fn [db [_ product-id]]
    (let [selected-products     (into #{} (->> (d/pull db '[:booking/selected-products] app-datom-id)
                                               :booking/selected-products
                                               (map :db/id)))
          new-selected-products (-> (union selected-products #{product-id})
                                    (difference (intersection #{product-id} selected-products)))]
      (-> db
          (d/db-with (->> selected-products
                          (map #(vec [:db/retract app-datom-id :booking/selected-products %]))))
          (d/db-with (->> new-selected-products
                          (map #(vec [:db/add app-datom-id :booking/selected-products %]))))))))

(reg-event-db
  :booking/set-time
  [rp/recordable]
  (fn [db [_ time]]
    (d/db-with db [[:db/add app-datom-id :booking/time time]])))

(reg-event-fx
  :booking/address-search
  [rp/recordable]
  (fn [{:keys [db]} [_ address-string]]
    (let [location (-> (d/entity db app-datom-id)
                       :booking/location)]
      {:db (d/db-with db (concat [[:db/add app-datom-id :booking/address-search address-string]]
                                 (when location
                                   [[:db/retract app-datom-id :booking/location location]])))
       :fetch (-> {:method :get
                   :uri (-> (url "https://maps.googleapis.com/maps/api/geocode/json")
                            (assoc :query {:region "gb"
                                           :address address-string
                                           :key google-maps-key})
                            str)
                   :on-success [:booking/address-search-results]}
                  json-request)})))

(reg-event-db
  :booking/address-search-results
  [rp/recordable]
  (fn [db [_ results]]
    (let [parsed-results (->> results
                              :results
                              (map (fn [address]
                                     {:address (:formatted_address address)
                                      :location (-> address
                                                    (get-in [:geometry :location])
                                                    ((juxt :lat :lng)))})))]
      (d/db-with db [[:db/add app-datom-id :booking/address-possibilities parsed-results]]))))

(reg-event-db
  :booking/location-selected
  [rp/recordable]
  (fn [db [_ location]]
    (d/db-with db [[:db/add app-datom-id :booking/location location]])))

(reg-event-fx
  :booking/submit
  [rp/recordable]
  (fn [{:keys [db]} _]
    (let [booking-data  (d/pull db [:booking/location :booking/stylist-id :booking/time {:booking/selected-products [:product/type]}] app-datom-id)
          [lat lng] (:booking/location booking-data)
          product-types (map :product/type (:booking/selected-products booking-data))]
      {:db db
       :fetch (-> {:method :post
                   :uri (str api-url "/appointments")
                   :body {:location/lat lat
                          :location/lng lng
                          :appointment/stylist (:booking/stylist-id booking-data)
                          :appointment/time (:booking/time booking-data)
                          :appointment/product-types product-types}
                   :on-success [:booking/submitted]
                   :on-failure [:booking/submit-failed]}
                  (add-auth-token db)
                  edn-request
                  handle-not-authorised)})))

(reg-event-fx
  :booking/submitted
  [rp/recordable]
  (fn [{:keys [db]} [_ {:keys [main related]}]]
    {:db (-> db
             (d/db-with (concat main related))
             (update :routing #(-> %
                                   js->clj
                                   (update "routes" butlast)
                                   (update "index" dec)
                                   clj->js)))
     :toast {:text "Appointment request submitted!"}}))

(reg-event-fx
  :booking/submit-failed
  [rp/recordable]
  (fn [{:keys [db]} [_ {:keys [explanation]}]]
    (let [validation-messages (->> explanation
                                   validation/spec-error->validation-messages
                                   (map (fn [e]
                                          (if (= e ::validation/unknown-error)
                                            "Unknown error - please contact support"
                                            e))))]
      {:db (d/db-with db [[:db/add app-datom-id :booking/errors validation-messages]])})))

(reg-event-fx
  :appointment/update-status
  [rp/recordable]
  (fn [{:keys [db]} [_ appointment-id new-status]]
    {:db db
     :fetch (-> {:method :put
                 :uri (str api-url "/appointments/" appointment-id)
                 :body {:appointment/status new-status}
                 :on-success [:appointment/status-updated]}
                (add-auth-token db)
                edn-request
                handle-not-authorised)}))

(reg-event-fx
  :appointment/status-updated
  [rp/recordable]
  (fn [{:keys [db]} [_ {:keys [main]}]]
    {:db (d/db-with db main)
     :toast {:text "Appointment updated"}}))

(reg-event-fx
  :existing-appointments/refresh
  [rp/recordable]
  (fn [{:keys [db]} _]
    {:db (d/db-with db [[:db/add app-datom-id :existing-appointments/refreshing true]])
     :fetch (-> {:method :get
                 :uri (str api-url "/appointments")
                 :on-success [:appointments-received]}
                (add-auth-token db)
                (edn-request)
                (handle-not-authorised))}))


(reg-event-fx
  :link/clicked
  [rp/recordable]
  (fn [{:keys [db]} [_ url]]
    {:open-url url}))
