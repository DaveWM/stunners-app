(ns stunners-app.styles
  (:require [cljs-exponent.font :as font]))

(def primary-colour "#8e24aa")
(def primary-colour-light "#c158dc")
(def primary-colour-dark "#5c007a")

(def secondary-colour "#4527a0")
(def secondary-colour-light "#7953d2")
(def secondary-colour-dark "#000070")

(def bad-colour "#D32F2F")

(def normal-font "fs-lola")
(def bold-font "elsie-black")

