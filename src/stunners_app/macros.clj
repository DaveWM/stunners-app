(ns stunners-app.macros
  (:require [clojure.string :as str]))

(defn camel-case->kebab-case [s]
  (let [camel-case-regex #"(?=[A-Z])"]
    (->> (str/split s camel-case-regex)
         (map str/lower-case)
         (str/join "-"))))

(defmacro def-nb-components [component-names]
  `(let [~'native-base (js/require "native-base")

         ~'nb-comp (fn [~'name]
                   (-> ~'native-base
                       (aget ~'name)
                       reagent.core/adapt-react-class))]

     ~@(->> component-names
            (map (fn [c]
                   `(def ~(symbol (camel-case->kebab-case c)) (~'nb-comp ~c)))))))
