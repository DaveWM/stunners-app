(ns stunners-app.expo-helpers
  (:require [stunners-app.react-native :refer [os]]
            [cljs-exponent.reagent :refer [touchable-native-feedback touchable-highlight]]
            [cljs-exponent.core :refer [exponent]]))

(def touchable (if (= os :android)
                 touchable-native-feedback
                 touchable-highlight))

(defn start-auth-session [options]
  (-> exponent
      (aget "AuthSession")
      (aget "startAsync")
      (.call nil (clj->js options))))

(defn get-redirect-url []
  (-> exponent
      (aget "AuthSession")
      (aget "getRedirectUrl")
      (.call nil)))

