(ns stunners-app.pages.new-appointment
  (:require [stunners-app.native-base :as nb]
            [cljs-exponent.reagent :refer [text view list-view image]]
            [stunners-app.styles :as styles]
            [stunners-app.components :refer [products-display]]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [stunners-app.expo-helpers :refer [touchable]]
            [stunners-app.react-native :refer [touchable-background icon]]
            [stunners-app.utilities :as utils]))

(defn stylist-filters [filter-groups selected-filter-group]
  [nb/card {:style {:flex 0
                 :margin-left 10
                 :margin-right 10
                 :margin-bottom 15}}
   [nb/card-item {:header true
               :style {:padding-top 12
                       :padding-bottom 8}}
    [text {:style {:font-family styles/bold-font
                   :color styles/secondary-colour}}
     "What do you want doing?"]]
   [nb/card-item {:style {:padding-top 0
                   :padding-bottom 0}}
    [nb/body {:style {:flex-direction "row"
                      :justify-content "center"
                      :flex-wrap "wrap"
                      :padding-right 12
                      :padding-left 12}}
     (->> filter-groups
          (map merge (->> [styles/secondary-colour styles/primary-colour styles/primary-colour-dark styles/secondary-colour-dark]
                          (map (partial hash-map :colour))))
          (map (fn [{:keys [db/id filter-group/label colour]}]
                 (let [selected (= id selected-filter-group)]
                   ^{:key label}
                   [nb/button {:rounded true
                            :style {:background-color (if selected colour nil)
                                    :border-color (if selected nil colour)
                                    :margin-left 5
                                    :margin-right 5
                                    :margin-bottom 5
                                    :padding 15}
                            :small true
                            :bordered (not selected)
                            :on-press #(rf/dispatch [:select-filter-group id])}
                    [text {:style {:color (if selected "white" colour)
                                   :font-family styles/normal-font}}
                     label]]))))]]])

(defn stylist-list-item [{products :product/_stylist
                          :keys [db/id user/name location/outcode user/avatar stylist/headline]}]
 ^{:key name}
  [touchable {:background (touchable-background)
              :on-press #(rf/dispatch [:start-booking id (map :id products)])}
   [view {:style {:flex-direction "column"
                  :padding-left 8
                  :padding-right 8}}
    [view {:style {:flex-direction "row"
                   :padding-top 12
                   :padding-bottom 12}}
     [view {:style {:flex-direction "column"
                    :justify-content "space-around"
                    :align-items "center"
                    :margin-right 8}}
      [image {:source {:uri avatar}
              :resize-mode "cover"
              :style {:width 40
                      :height 40
                      :border-radius 20}}]]
     [view {:style {:flex-direction "column"
                    :justify-content "flex-start"
                    :align-items "flex-start"
                    :padding-left 10
                    :padding-right 15
                    :flex 1}}
      [text {:style {:font-size 18
                     :font-family styles/bold-font
                     :text-align "left"}}
       name]
      [text {:style {:font-size 14
                     :font-family styles/normal-font
                     :flex 1
                     :text-align "left"}}
       (str "\"" headline "\"")]]
     [view {:style {:flex-direction "column"
                    :justify-content "space-around"
                    :align-items "center"
                    :padding-right 5
                    :min-width 50}}
      [view {:style {:flex-direction "row"
                     :align-items "center"}}
       [icon {:name "md-pin"
              :size 12
              :color "black"
              :style {:margin-right 3}}]
       [text {:style {:font-family styles/normal-font}} outcode]]
      ]]
    [products-display products]]])

(defn page []
  (let [stylists (rf/subscribe [:new-appointment/get-stylists])
        filter-groups (rf/subscribe [:get-filter-groups])
        page-data (rf/subscribe [:new-appointment-page])]
    (fn []
      [view {:style {:flex 1}}
       [stylist-filters @filter-groups (get-in @page-data
                                               [:new-appointment/selected-filter-group :db/id])]
       (if (:new-appointment/selected-filter-group @page-data)
         (if (< 0 (count @stylists))
           [list-view {:style {:flex 1}
                       :content-container-style {:align-items "stretch"}
                       :dataSource (utils/to-data-source @stylists)
                       :render-row (fn [data]
                                     (r/as-element [stylist-list-item (js->clj data :keywordize-keys true)]))
                       :render-separator #(r/as-element [view {:style {:margin-left 20
                                                                       :margin-right 20
                                                                       :opacity 0.3
                                                                       :border-bottom-color "grey"
                                                                       :border-bottom-width 1
                                                                       :background-color "transparent"}}])
                       :enable-empty-sections true}]
           [text {:style {:text-align "center"
                          :padding-left 30
                          :padding-right 30
                          :font-size 20
                          :color styles/primary-colour
                          :font-family styles/normal-font}}
            "Sorry, there aren't any stylists available to do that at the moment  :("])
         [text {:style {:text-align "center"
                        :padding-left 30
                        :padding-right 30
                        :font-size 20
                        :color styles/primary-colour
                        :font-family styles/normal-font}}
          "Want to be stunning? Select what you want doing above, and we'll show you which stylists are available"])])))
