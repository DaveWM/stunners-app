(ns stunners-app.pages.booking
  (:require [cljs-exponent.reagent :refer [text view image scroll-view]]
            [stunners-app.react-native :refer [icon touchable-background]]
            [re-frame.core :as rf]
            [stunners-app.native-base :as nb]
            [stunners-app.styles :as styles]
            [cljs-exponent.font :as font]
            [reagent.core :as r]
            [stunners-app.expo-helpers :refer [touchable]]
            [stunners-app.components :as components]))

(def swiper (->> (js/require "react-native-swiper")
                 r/adapt-react-class))

(def date-picker (-> (js/require "react-native-datepicker")
                     (aget "default")
                     r/adapt-react-class))

(defn page []
  (let [stylist (rf/subscribe [:booking/stylist-info])
        products (rf/subscribe [:booking/products])
        time (rf/subscribe [:booking/time])
        booking-location (rf/subscribe [:booking/location])
        address-possibilities (rf/subscribe [:booking/address-possibilities])
        booking-errors (rf/subscribe [:booking/errors])]
    (fn []
      [scroll-view {:style {:background-color "white"
                            :flex 1}}
       [view {:style {:padding 15
                      :background-color styles/primary-colour
                      :flex-direction "row"
                      :align-items "center"}}
        [icon {:name "md-arrow-round-back"
               :size 24
               :color "white"
               :style {:margin-right 15
                       :margin-top 3}
               :on-press #(rf/dispatch [:cljs-react-navigation.re-frame/goBack])}]
        [view {:style {:flex-shrink 1}}
         [nb/h1 {:style {:font-family styles/bold-font
                         :margin-bottom 4
                         :color "white"}}
          (:user/name @stylist)]
         [nb/h3 {:style {:font-family styles/normal-font
                         :font-size 18
                         :color "white"}}
          (:stylist/headline @stylist)]]]
       [view {:style {:height 280}}
        [swiper {:autoplay true
                 :autoplay-timeout 6
                 :dot-color styles/primary-colour-dark
                 :active-dot-color styles/primary-colour-light}
         (map (fn [image-url]
                [image {:source {:uri image-url}
                        :key image-url
                        :style {:flex 1}}])
              (:stylist/images @stylist))]]
       [view {:style {:margin-top 12
                      :margin-bottom 12
                      :padding-left 25
                      :padding-right 25}}
        [text {:style {:font-family styles/normal-font
                       :font-size 15}}
         (:stylist/description @stylist)]]
       [view {:style {:padding-left 6
                      :padding-right 6}}
        [nb/h3 {:style {:font-family styles/bold-font
                        :margin-bottom 18}}
         "What do you want?"]
        [view {:style {:flex-direction "column"}}
         (->> @products
              (partition-all 3)
              (map (fn [row-products]
                     [view {:key (gensym)
                            :style {:flex-direction "row"
                                    :flex 1
                                    :align-items "stretch"
                                    :justify-content "center"}}
                      (->> row-products
                           (map (fn [{:keys [id name cost selected]}]
                                  [view {:key id
                                         :style {:flex 1
                                                 :flex-direction "row"
                                                 :justify-content "flex-start"
                                                 :align-items "center"}}
                                   [nb/check-box {:checked selected
                                                 :color styles/primary-colour
                                                 :on-press #(rf/dispatch [:booking/product-toggled id])}]
                                   [view {:style {:margin-left 19}}
                                    [nb/text {:style {:font-family styles/normal-font}}
                                     name]
                                    [nb/text {:style {:font-family styles/normal-font
                                                      :opacity 0.8}}
                                     (str "£" cost)]]])))])))]
        [view {:style {:border-top-color "rgba(0,0,0, 0.25)"
                       :border-top-width 1
                       :margin-left 15
                       :margin-right 15
                       :margin-top 15
                       :padding-top 8
                       :align-items "center"}}
         [text {:style {:font-family styles/normal-font
                        :font-size 16}}
          (str "Total: £" (->> @products
                               (filter :selected)
                               (map :cost)
                               (reduce + 0)))]]]

       [view {:style {:padding-left 6
                      :padding-right 6
                      :margin-top 4}}
        [nb/h3 {:style {:font-family styles/bold-font
                        :margin-bottom 18}}
         "When?"]
        [view {:style {:padding-left 8
                       :flex-direction "row"
                       :align-items "center"}}
         [icon {:name "md-calendar"
              :size 32
              :color "black"
              :style {:margin-right 10}}]
         [date-picker {:date (when @time (js/Date. @time))
                       :placeholder "Click me"
                       :format "DD/MM/YYYY HH:mm"
                       :mode "datetime"
                       :show-icon false
                       :custom-styles (clj->js {:dateText {:fontFamily styles/normal-font}})
                       :on-date-change (fn [date-string]
                                         (let [regex #"(\d{2})/(\d{2})/(\d{4})\s+(\d{2}):(\d{2})"
                                               [_ day month year hour minute] (re-find regex date-string)
                                               time (-> (js/Date. year (dec month) day hour minute 0 0))]
                                           (rf/dispatch [:booking/set-time time])))}]]]

       (let [booking-location @booking-location]
         [view {:style {:padding-left 6
                        :padding-right 6
                        :margin-top 12}}
          [nb/h3 {:style {:font-family styles/bold-font}}
           "Where?"]
          [nb/item {:underline true
                    :last true}
           (when booking-location
             [icon {:name "md-checkmark"
                    :color "green"}])
           [nb/input {:placeholder "Start typing your address here"
                      :on-change-text #(rf/dispatch [:booking/address-search %])
                      :style {:font-family styles/normal-font}}]]
          (when @address-possibilities
            [nb/list
             (map (fn [{:keys [address location]}]
                    [nb/list-item {:key location
                                   :on-press #(rf/dispatch [:booking/location-selected location])
                                   :icon true
                                   :style {:background-color "rgba(0,0,0,0)"}
                                   :button true}
                     [nb/left {:style {:background-color "rgba(0,0,0,0)"}}
                      (when (= location booking-location)
                        [icon {:name "md-checkmark"
                               :color "green"
                               :style {:background-color "rgba(0,0,0,0)"}}])]
                     [nb/body
                      [text {:style {:font-family styles/normal-font}} address]]])
                  @address-possibilities)])])

       (when-let [errors @booking-errors]
         [components/error-display errors])

       [nb/button {:large true
                   :rounded true
                   :block true
                   :style {:margin-top 20
                           :margin-bottom 3
                           :margin-left 3
                           :margin-right 3
                           :background-color styles/primary-colour}
                   :on-press #(rf/dispatch [:booking/submit])}
        [text {:style {:font-family styles/normal-font
                       :color "white"
                       :font-size 22}}
         "Book my Appointment"]]])))
