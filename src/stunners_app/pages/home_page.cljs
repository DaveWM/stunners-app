(ns stunners-app.pages.home-page
  (:require [cljs-exponent.reagent :refer [text view image activity-indicator] :as rn]
            [stunners-app.native-base :as nb]
            [re-frame.core :refer [subscribe dispatch]]
            [stunners-app.styles :as styles]
            [stunners-app.pages.new-appointment :as new-appointment]
            [stunners-app.pages.existing-appointments :as existing-appointments]
            [stunners-app.pages.registration :as registration]))

(defn page []
  (let [loading? (subscribe [:loading])
        authenticated? (subscribe [:authenticated])
        current-user (subscribe [:current-user])]
    (fn []
      [nb/container {:style {:flex-direction "column"
                             :align-items "stretch"
                             :flex 1}}
       [nb/header {:style {:background-color styles/primary-colour
                           :padding 5}
                   :has-tabs true}
        [rn/status-bar {:background-color styles/primary-colour
                        :bar-style "light-content"
                        :android-status-bar-color "#8e24aa"}]
        [nb/body {:style {:flex-direction "column"
                          :align-items "center"}}
         [image {:source (js/require "./assets/images/logo.png")
                 :style {:flex 1
                         :resize-mode "contain"}}]]]
       (cond
         @loading? [nb/body {:style {:flex-direction "column"
                                     :align-items "center"
                                     :justify-content "center"
                                     :flex 1}}
                    [activity-indicator {:size "large"
                                         :color styles/primary-colour}]]
         (not @authenticated?) [nb/body {:style {:flex-direction "column"
                                                 :align-items "center"
                                                 :justify-content "center"
                                                 :flex 1}}
                                [nb/button {:style {:background-color styles/primary-colour
                                                    :padding 7}
                                            :on-press #(dispatch [:log-in])}
                                 [text {:style {:font-family styles/normal-font
                                                :color "white"
                                                :font-size 22}}
                                  "Log in"]]]

         (nil? @current-user) [registration/page]

         :else [nb/tabs {:initial-page 0
                         :background-color styles/primary-colour
                         :tab-bar-underline-style {:background-color styles/primary-colour-dark}}
                (->> [[:new-appointment "Book a Stylist" new-appointment/page]
                      [:existing-appointments "My Appointments" existing-appointments/page]]
                     (map (fn [[route heading component]]
                            ^{:key route}
                            [nb/tab {:heading heading
                                     :tab-style {:background-color styles/primary-colour}
                                     :active-tab-style {:background-color styles/primary-colour}
                                     :text-style {:color "white"
                                                  :opacity 0.8
                                                  :font-family styles/normal-font}
                                     :active-text-style {:font-weight "normal"
                                                         :opacity 1
                                                         :font-family styles/normal-font}}
                             [component]])))]
         )])))
