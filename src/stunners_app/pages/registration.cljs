(ns stunners-app.pages.registration
  (:require [re-frame.core :as rf]
            [stunners-app.native-base :as nb]
            [cljs-exponent.reagent :refer [text view image scroll-view modal]]
            [stunners-app.styles :as styles]
            [stunners-app.components :as components]))

(defn page []
  (let [registration-info   (rf/subscribe [:registration])
        registration-errors (rf/subscribe [:registration/errors])]
    (fn []
      (let [{:keys [registration/name registration/phone-number registration/email registration/terms-accepted]} @registration-info]
        [view {:style {:background-color "white"
                       :flex 1
                       :padding 15}}
         [nb/h1 {:style {:font-family styles/bold-font
                         :margin-bottom 20}}
          "Enter your info"]
         [scroll-view {:style {:flex 1}}
          (->> [["Your full name" name :registration/name-changed]
                ["Phone number" phone-number :registration/phone-number-changed "phone-pad"]
                ["Email" email :registration/email-changed "email-address"]]
               (map (fn [[label value on-changed type]]
                       [nb/item {:stackedLabel true
                                 :style {:margin-bottom 10}
                                 :key label}
                        [nb/label {:style {:font-family styles/normal-font}}
                         label]
                        [nb/input {:on-change-text #(rf/dispatch-sync [on-changed %])
                                   :default-value value
                                   :keyboardType (or type "default")
                                   :style {:font-family styles/normal-font}}]])))
          [view {:style {:flex-direction "row"
                         :align-items "center"
                         :margin-top 15}}
           [nb/check-box {:checked terms-accepted
                          :color styles/primary-colour
                          :on-press #(rf/dispatch-sync [:registration/terms-accepted-changed %])
                          :style {:padding-top 2}}]
           (let [link (fn [txt url] [text {:style {:color "blue"
                                                    :text-decoration-line "underline"}
                                           :on-press #(rf/dispatch [:link/clicked url])}
                                      txt])]
             [text {:style {:font-family styles/normal-font
                            :margin-left 15}}
              "Please confirm you accept our " (link "Terms and Conditions" "http://www.google.com") " and " (link "Privacy Policy" "http://www.google.com")])]]

         (when-let [errors @registration-errors]
           [components/error-display errors])

         [nb/button {:large true
                     :rounded true
                     :block true
                     :disabled (not terms-accepted)
                     :style {:margin-top 5
                             :background-color (if terms-accepted
                                                 styles/primary-colour
                                                 "gray")
                             :flex 0}
                     :on-press #(rf/dispatch [:registration/register])}
          [text {:style {:font-family styles/normal-font
                         :color "white"
                         :font-size 22}}
           "Register"]]]))))
