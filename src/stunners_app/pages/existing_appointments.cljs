(ns stunners-app.pages.existing-appointments
  (:require [clojure.string :as str]
            [reagent.core :as r]
            [re-frame.core :as rf]
            [stunners-app.components :refer [products-display]]
            [stunners-app.native-base :as nb]
            [cljs-exponent.reagent :refer [text view list-view]]
            [stunners-app.react-native :refer [icon refresh-control] :as rn]
            [stunners-app.utilities :as utils]
            [stunners-app.styles :as styles]
            [cljs-time.format :as time]
            [cljs-time.coerce]))

(defmulti card-title (fn [{:keys [is-current-user-stylist status]}]
                       [is-current-user-stylist status]))

(defmethod card-title [true :confirmed] [{:keys [stylee]}]
  (str "I'm styling " (:name stylee)))

(defmethod card-title [false :confirmed] [{:keys [stylist]}]
  (str "I'm being styled by " (:name stylist)))

(defmethod card-title [true :pending] [{:keys [stylee]}]
  (str "I've been requested to style " (:name stylee)))

(defmethod card-title [false :pending] [{:keys [stylist]}]
  (str "I've requested an appointment with " (:name stylist)))

(defmethod card-title [false :rejected] [_]
  "Your appointment request was rejected - sorry!")

(defmethod card-title [true :rejected] [{:keys [stylee]}]
  (str "You rejected an appointment with " (:name stylee)))

(defmethod card-title :default [appointment]
  "Unknown appointment")

(defmulti status-badge (fn [appointment]
                         (:status appointment)))

(defmethod status-badge :confirmed [_]
  [nb/badge {:style {:background-color styles/primary-colour-dark
                          :justify-content "center"
                          :align-items "center"}}
        [text {:style {:font-family styles/normal-font
                       :color "white"}}
         "Confirmed"]])

(defmethod status-badge :pending [_]
  [nb/badge {:style {:background-color styles/secondary-colour
                     :justify-content "center"
                     :align-items "center"}}
   [text {:style {:font-family styles/normal-font
                  :color "white"}}
    "Pending"]])

(defmethod status-badge :rejected [_]
  [nb/badge {:style {:background-color styles/bad-colour
                     :justify-content "center"
                     :align-items "center"}}
   [text {:style {:font-family styles/normal-font
                  :color "white"}}
    "Rejected"]])

(defn contact-info [user]
  [view
   [text {:style {:font-family styles/normal-font
                  :text-decoration-line "underline"
                  :font-size 17}} "Contact Info"]
   [view {:style {:padding-left 12
                  :margin-top 8}}
    [text {:style {:font-family styles/normal-font
                   :margin-bottom 6
                   :text-decoration-line "underline"
                   :color "#0000EE"}
           :on-press #(rn/open-url (str "tel:" (:phone-number user)))}
     [icon {:name "md-call"}] " " (:phone-number user)]
    [text {:style {:font-family styles/normal-font
                   :text-decoration-line "underline"
                   :color "#0000EE"}
           :on-press #(rn/open-url (str "mailto:" (:email user)))}
     [icon {:name "md-mail"}] " " (:email user)]]])

(defn card-footer-button [attrs label]
  (let [button-styles (merge {:justify-content "center"
                              :align-items "center"
                              :padding-left 12
                              :padding-right 12}
                             (:style attrs))
        button-attrs (-> (merge {:transparent true
                                 :small true
                                 :primary true
                                 :android-ripple-color styles/primary-colour-dark}
                                attrs)
                         (assoc :style button-styles))]
    [nb/button button-attrs
     [text {:style {:color styles/primary-colour}}
      (str/upper-case label)]]))


(defn appointment-card [{:keys [appointment]}]
  [nb/card {:style {:flex 0}}
    [nb/card-item
     [nb/left {:style {:flex 1}}
      [nb/thumbnail {:source {:uri (if (:is-current-user-stylist appointment)
                                     (get-in appointment [:stylee :avatar])
                                     (get-in appointment [:stylist :avatar]))}}]
      [nb/body
       [nb/text {:style {:font-family styles/normal-font}}
        (card-title appointment)]
       [nb/text {:note true
                 :style {:font-family styles/normal-font
                         :margin-bottom 6}}
        [icon {:name "md-time"}]
        " "
        (->> (:time appointment)
             cljs-time.coerce/to-date-time
             (time/unparse {:format-str "dd/MM/YYYY HH:mm"}))]]]
     [nb/right {:style {:flex 0}}
      [status-badge appointment]]]
   (when (= :confirmed (:status appointment))
     (let [contact-user (if (:is-current-user-stylist appointment) (:stylee appointment) (:stylist appointment))]
       [view {:style {:flex-direction "row"
                      :justify-content "center"
                      :align-items "center"
                      :flex-wrap "wrap"}}
        [nb/text {:style {:font-family styles/normal-font
                          :margin-right 8}
                  :note true
                  :on-press #(rn/open-url (str "tel:" (:phone-number contact-user)))}
         [icon {:name "md-call"}] " " (:phone-number contact-user)]
        [nb/text {:note true
                  :style {:font-family styles/normal-font}
                  :on-press #(rn/open-url (str "mailto:" (:email contact-user)))}
         [icon {:name "md-mail"}] " " (:email contact-user)]]))
   [nb/card-item {:style {:flex-direction "row"
                          :justify-content "space-around"
                          :align-items "center"
                          :flex 1}}
    (when (= :confirmed (:status appointment))
      [view {:style {:flex-direction "row"
                       :align-items "center"
                       :justify-content "center"
                       :flex 1}}
         [icon {:name "md-map" :size 16 :style {:margin-right 6}}]
         [view
          (->> (:address appointment)
               utils/split-comma
               (map str/trim)
               (map (fn [line] ^{:key line} [text {:style {:font-family styles/normal-font}} line])))]])
    (when (= :confirmed (:status appointment))
      [view {:style {:width 1
                     :align-self "stretch"
                     :background-color "grey"
                     :opacity 0.3}}])
    [view {:style {:flex 1}}
     [products-display (:products appointment)]]]

   (cond
     (= :confirmed (:status appointment))
     (let [contact-user (if (:is-current-user-stylist appointment) (:stylee appointment) (:stylist appointment))
           actions [["Call" (str "tel:" (:phone-number contact-user))]
                    ["Email" (str "mailto:" (:email contact-user))]
                    ["Directions" (str "geo:" (str (:lat appointment) "," (:lng appointment)))]]]
       [nb/card-item {:footer true}
        (->> actions
             (map (fn [[label link]]
                    [card-footer-button {:on-press #(rn/open-url link) :key label}
                     label])))])

     (and (:is-current-user-stylist appointment)
          (= :pending (:status appointment)))
     [nb/card-item {:footer true}
      (->> [[:appointment-status/confirmed "confirm"]
            [:appointment-status/rejected "reject"]]
           (map (fn [[status label]]
                  [card-footer-button {:key status
                                       :on-press #(rf/dispatch [:appointment/update-status (:id appointment) status])}
                   label])))])])

(defn page []
  (let [appointments (rf/subscribe [:existing-appointments/appointments])
        refreshing (rf/subscribe [:existing-appointments/refreshing])]
    (fn []
      (if (seq @appointments)
        [view {:style {:flex 1}}
         [list-view {:style {:flex 1}
                     :content-container-style {:align-items "stretch"}
                     :dataSource (utils/to-data-source @appointments)
                     :render-row (fn [appointment]
                                   (r/as-element [appointment-card {:appointment (-> appointment
                                                                                     (js->clj :keywordize-keys true)
                                                                                     (update :status keyword))}]))
                     :enable-empty-sections true
                     :refresh-control (r/as-element [refresh-control
                                                     {:on-refresh #(rf/dispatch [:existing-appointments/refresh])
                                                      :refreshing @refreshing
                                                      :colors [styles/primary-colour]}])}]]
        [view {:style {:flex 1
                       :align-items "center"
                       :justify-content "center"}}
         [text {:style {:text-align "center"
                        :padding-left 30
                        :padding-right 30
                        :font-size 20
                        :color styles/primary-colour
                        :font-family styles/normal-font}}
          "You don't have any appointments at the moment"]]))))
