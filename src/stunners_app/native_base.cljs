(ns stunners-app.native-base
  (:require [reagent.core :as r])
  (:require-macros [stunners-app.macros :refer [def-nb-components]]))

(def-nb-components ["Container"
                    "Content"
                    "Header"
                    "Left"
                    "Right"
                    "Body"
                    "Tabs"
                    "Tab"
                    "Card"
                    "CardItem"
                    "Button"
                    "Text"
                    "Thumbnail"
                    "H1"
                    "H2"
                    "H3"
                    "CheckBox"
                    "List"
                    "ListItem"
                    "Item"
                    "Label"
                    "Input"
                    "Root"
                    "Badge"])

(def Toast (-> (js/require "native-base")
               (aget "Toast")))
