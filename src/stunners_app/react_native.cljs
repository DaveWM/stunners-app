(ns stunners-app.react-native
  (:require [cljs-exponent.core :refer [react-native]]
            [reagent.core :as r]))

(def os (-> react-native
            (aget "Platform")
            (aget "OS")
            keyword))

(def icon (-> (js/require "@expo/vector-icons")
              (aget "Ionicons")
              r/adapt-react-class))

(def data-source (-> (aget react-native "ListView")
                     .-DataSource))

(def touchable-background (-> react-native
                              (aget "TouchableNativeFeedback")
                              (aget "SelectableBackground")))

(def refresh-control (-> react-native
                         (aget "RefreshControl")
                         r/adapt-react-class))

(def open-url
  (let [linking (-> react-native
                    (aget "Linking"))]
    (-> (.-openURL linking)
        (.bind linking))))

(defn add-back-handler [handler]
  (-> react-native
      (aget "BackHandler")
      (.addEventListener "hardwareBackPress" handler)))
