(ns stunners-app.components
  (:require [cljs-exponent.reagent :refer [view text ]]
            [stunners-app.react-native :refer [icon]]
            [stunners-app.styles :as styles]))

(defn products-display [products]
  [view {:style {:flex-direction "row"
                 :justify-content "center"
                 :align-items "stretch"
                 :margin-bottom 8}}
   [view {:style {:flex-direction "column"
                  :flex 1
                  :align-items "flex-end"
                  :justify-content "center"
                  :border-right-color styles/secondary-colour-light
                  :border-right-width 1
                  :padding-right 12}}
    (map (fn [{:keys [id cost type]}]
           ^{:key id}
           [view {:style {:flex-direction "row"
                          :justify-content "center"
                          :align-items "center"
                          :flex-wrap "wrap"}}
            [text {:style {:text-align "right"
                           :margin-right 12
                           :font-family styles/normal-font}}
             type]
            [text {:style {:text-align "left"
                           :opacity 0.6}}
             (str "£" cost)]])
         products)]
   [view {:style {:flex-direction "column"
                  :align-items "flex-start"
                  :justify-content "center"
                  :flex 1
                  :padding-left 12}}
    [text {:style {:font-size 8
                   :opacity 0.6
                   :font-family styles/normal-font}}
     "Total"]
    [text {:style {:font-family styles/normal-font}}
     (str "£" (->> products
                   (map :cost)
                   (reduce +)))]]])

(defn error-display [errors]
  [view {:style {:background-color styles/bad-colour
                        :padding 8
                        :margin 12
                        :border-radius 15}}
          [text {:style {:font-family styles/bold-font
                         :font-size 20
                         :color "white"}}
           [icon {:name "md-alert"
                  :size 20
                  :color "white"}]
           " Oops, something went wrong"]
          [view {:style {:padding 8
                         :margin-top 4}}
           (->> errors
                (map (fn [e]
                       ^{:key e} [text {:style {:color "white"
                                                :font-family styles/normal-font}}
                                  (str "• " e)])))]])

