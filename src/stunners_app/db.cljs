(ns stunners-app.db
  (:require [datascript.core :as d]))

(def app-datom-id 0)

(def schema {:product/stylist {:db/valueType :db.type/ref
                               :db/cardinality :db.cardinality/one}
             :new-appointment/selected-filter-group {:db/valueType :db.type/ref
                                                     :db/cardinality :db.cardinality/one}
             :appointment/product-types {:db/cardinality :db.cardinality/many}
             :appointment/stylist {:db/valueType :db.type/ref
                                   :db/cardinality :db.cardinality/one}
             :appointment/stylee {:db/valueType :db.type/ref
                                  :db/cardinality :db.cardinality/one}
             :stylist/products {:db/valueType :db.type/ref
                                :db/cardinality :db.cardinality/many}
             :stylist/images {:db/cardinality :db.cardinality/many}
             :booking/selected-products {:db/valueType :db.type/ref
                                         :db/cardinality :db.cardinality/many}
             :app/current-user {:db/valueType :db.type/ref
                                :db/cardinality :db.cardinality/one}
             :filter-group/product-types {:db/cardinality :db.cardinality/many}})

(def app-db
  (-> (d/empty-db schema)
      (d/db-with [[:db/add app-datom-id :app/loading false]

                  [:db/add -6 :filter-group/label "hair"]
                  [:db/add -6 :filter-group/product-types :product-type/haircut]
                  [:db/add -7 :filter-group/label "nails"]
                  [:db/add -7 :filter-group/product-types :product-type/nails]
                  [:db/add -8 :filter-group/label "body"]
                  [:db/add -8 :filter-group/product-types :product-type/waxing]
                  [:db/add -9 :filter-group/label "full package"]
                  [:db/add -9 :filter-group/product-types :product-type/haircut]
                  [:db/add -9 :filter-group/product-types :product-type/nails]
                  [:db/add -9 :filter-group/product-types :product-type/waxing]])))
