(ns stunners-app.subs
  (:require [re-frame.core :refer [reg-sub]]
            [stunners-app.db :refer [app-datom-id]]
            [datascript.core :as d]
            [clojure.set :refer [subset? superset?]]))

(reg-sub
 :new-appointment/get-stylists
 (fn [db _]
   (let [filtered-products (->> (d/q '[:find [?p ...]
                                       :where
                                       [0 :new-appointment/selected-filter-group ?fg]
                                       [?fg :filter-group/product-types ?p]]
                                     db)
                                (into #{}))
         where-clauses (->> filtered-products
                            (mapcat #(let [var (gensym '?)]
                                       [[var :product/stylist '?e]
                                        [var :product/type %]])))]
     (->> (d/q (apply conj
                      '[:find
                        [(pull ?e [:db/id :user/name :user/avatar :stylist/headline :location/outcode {:product/_stylist [:db/id :product/cost :product/type]}]) ...]
                        :where
                        [?e :stylist/headline]]
                      where-clauses)
               db)
          (map #(update % :product/_stylist (partial filter (fn [p]
                                                              (contains? filtered-products (:product/type p))))))))))

(reg-sub
 :loading
 (fn [db _]
   (let [app-entity (d/entity db app-datom-id)]
     (or (:app/loading app-entity)
         (not (:app/fonts-loaded app-entity))))))

(reg-sub
 :authenticated
 (fn [db _]
   (->> db
        (d/q '[:find ?token .
               :where [0 :auth/token ?token]])
        boolean)))

(reg-sub
 :current-user
 (fn [db _]
   (d/q '[:find (pull ?e [:user/name]) .
          :where [_ :app/current-user ?e]]
        db)))

(reg-sub
 :get-filter-groups
 (fn [db _]
   (d/q '[:find [(pull ?e [:db/id :filter-group/label]) ...]
          :where [?e :filter-group/label]]
        db)))

(reg-sub
 :new-appointment-page
 (fn [db _]
   (let [app-entity (d/entity db app-datom-id)]
     (select-keys app-entity [:new-appointment/selected-filter-group]))))

(reg-sub
 :booking/stylist-info
 (fn [db _]
   (d/q '[:find (pull ?s [:db/id :user/name :user/avatar :stylist/headline :stylist/description :stylist/cover-photo :stylist/images]) .
         :where [0 :booking/stylist-id ?s]]
        db)))

(reg-sub
 :booking/products
 (fn [db _]
   (let [selected-products (into #{} (->> (d/pull db '[:booking/selected-products] app-datom-id)
                                          :booking/selected-products
                                          (map :db/id)))]
     (->> (d/q '[:find ?p ?type ?cost ?selected
                 :where
                 [0 :booking/stylist-id ?s]
                 [?p :product/stylist ?s]
                 [?p :product/cost ?cost]
                 [?p :product/type ?type]
                 [(contains? ?selected-products ?p) ?selected]
                 :in $ ?selected-products]
               db selected-products)
          (map (partial zipmap [:id :name :cost :selected]))))))

(reg-sub
 :booking/time
 (fn [db _]
   (-> (d/entity db app-datom-id)
       :booking/time)))

(reg-sub
 :booking/location
 (fn [db _]
   (-> (d/entity db app-datom-id)
       :booking/location)))

(reg-sub
 :booking/address-possibilities
 (fn [db _]
   (-> (d/entity db app-datom-id)
       :booking/address-possibilities)))

(reg-sub
 :booking/errors
 (fn [db _]
   (-> (d/entity db app-datom-id)
       :booking/errors)))

(reg-sub
 :existing-appointments/appointments
 (fn [db _]
   (let [has-appointment-rule '[[(has-appointment ?u ?a)
                                 [?a :appointment/stylist ?u]]
                                [(has-appointment ?u ?a)
                                 [?a :appointment/stylee ?u]]]]
     (->> (d/q '[:find  ?a ?address ?status ?is-stylist (pull ?stylist [:user/name :user/avatar :user/phone-number :user/email]) (pull ?stylee [:user/name :user/avatar :user/phone-number :user/email]) ?time ?lat ?lng
                 :in $ %
                 :where
                 [0 :app/current-user ?u]
                 (has-appointment ?u ?a)
                 [?u :user/name ?name]
                 [?a :appointment/stylist ?stylist]
                 [?a :appointment/stylee ?stylee]
                 [?a :location/address ?address]
                 [?a :appointment/status ?status]
                 [?a :appointment/time ?time]
                 [?a :location/lat ?lat]
                 [?a :location/lng ?lng]
                 [(= ?u ?stylist) ?is-stylist]]
               db has-appointment-rule)
          (map (partial zipmap [:id :address :status :is-current-user-stylist :stylist :stylee :time :lat :lng]))
          (map (fn [{id :id :as appointment}]
                 (let [products (d/q '[:find [(pull ?p [:db/id :product/cost :product/type]) ...]
                                       :in $ ?a
                                       :where [?a :appointment/product-types ?pt]
                                       [?a :appointment/stylist ?s]
                                       [?p :product/stylist ?s]
                                       [?p :product/type ?pt]]
                                     db id)]
                   (assoc appointment :products products))))
          (sort-by :time >)))))

(reg-sub
 :existing-appointments/refreshing
 (fn [db _]
   (let [app-entity (d/entity db app-datom-id)]
     (:existing-appointments/refreshing app-entity))))

(reg-sub
 :registration
 (fn [db _]
   (d/pull db '[:registration/name :registration/email :registration/phone-number :registration/terms-accepted] app-datom-id)))

(reg-sub
 :registration/errors
 (fn [db _]
   (-> (d/entity db app-datom-id)
       :registration/errors)))
