(ns stunners-app.jwt)

(def jwt-decode (js/require "jwt-decode"))

(defn decode [token]
  (-> token
      jwt-decode
      (js->clj :keywordize-keys true)))
