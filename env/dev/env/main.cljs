(ns ^:figwheel-no-load env.main
  (:require [reagent.core :as r]
            [stunners-app.core :as core]
            [figwheel.client :as figwheel :include-macros true]
            [env.dev]
            [re-frame.loggers :as rf.log]))

(enable-console-print!)

(def cnt (r/atom 0))
(def warn (js/console.warn.bind js/console))
(defn reloader [] @cnt [core/app-root])
(def root-el (r/as-element [reloader]))

(figwheel/watch-and-reload
 :websocket-url (str "ws://" env.dev/ip ":3449/figwheel-ws")
 :heads-up-display false
 :jsload-callback #(do (swap! cnt inc)
                       (rf.log/set-loggers!
                        {:warn (fn [& args]
                                 (cond
                                   (= "re-frame: overwriting " (first args)) nil
                                   :else (apply warn args)))})))

(core/init)
