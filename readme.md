## stunners-app

### Usage

#### Install Expo [XDE and mobile client](https://expo.io/learn)

``` shell
    npm install expo-cli --global
```

#### Install [Lein](http://leiningen.org/#install)

#### Install npm modules

``` shell
    yarn install
```

#### Signup using exp CLI

``` shell
    expo register
```

#### Start the figwheel server and cljs repl

##### leiningen users
``` shell
    lein figwheel
```

#### Start Exponent server (Using `expo`)

##### Also connect to Android device

``` shell
    expo start
```

### Add new assets or external modules
1. `require` module:

``` clj
    (def cljs-logo (js/require "./assets/images/cljs.png"))
    (def FontAwesome (js/require "@exponent/vector-icons/FontAwesome"))
```
2. Reload simulator or device

### Make sure you disable live reload from the Developer Menu, also turn off Hot Module Reload.
Since Figwheel already does those.

### Production build (generates js/externs.js and main.js)

#### leiningen users
``` shell
lein prod-build
```
